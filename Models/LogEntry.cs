﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingServiceController.Models
{
    public class LogEntry
    {
        public string IdLogEntry { get; set; }
        public string NameLogEntry { get; set; }
        public string IdLogState { get; set; }
        public string NameLogState { get; set; }
        public string IdUser { get; set; }
        public string NameUser { get; set; }
        public string IdAttributeDateTimeStamp { get; set; }
        public DateTime? DateTimeStamp { get; set; }
        public string IdAttributeMessage { get; set; }
        public string Message { get; set; }
        public string IdRef { get; set; }

    }
}
