﻿using LoggingServiceController.Notifications;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LoggingServiceController.Services
{
    public class ElasticServiceAgent : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReaderLogEntries;
        private OntologyModDBConnector dbReaderLogStates;
        private OntologyModDBConnector dbReaderDateTimeStamps;
        private OntologyModDBConnector dbReaderMessages;
        private OntologyModDBConnector dbReaderUsers;


        private List<clsOntologyItem> refItems;
        private clsOntologyItem direction;
        private clsOntologyItem relationType;

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        public List<clsObjectRel> LogEntries
        {
            get
            {
                return dbReaderLogEntries.ObjectRels;
            }
        }
        public List<clsObjectRel> LogStates
        {
            get
            {
                return dbReaderLogStates.ObjectRels;
            }
        }

        public List<clsObjectRel> Users
        {
            get
            {
                return dbReaderUsers.ObjectRels;
            }
        }

        public List<clsObjectAtt> DateTimeStamps
        {
            get
            {
                return dbReaderDateTimeStamps.ObjAtts;
            }
        }

        public List<clsObjectAtt> Messages
        {
            get
            {
                return dbReaderMessages.ObjAtts;
            }
        }

        private Thread readDataAsync;

        public void Abort()
        {
            if (readDataAsync != null)
            {
                try
                {
                    readDataAsync.Abort();
                }
                catch(Exception ex)
                {

                }
            }
        }

        public clsOntologyItem GetLogEntries(List<clsOntologyItem> refItems, clsOntologyItem direction, clsOntologyItem relationType)
        {
            this.refItems = refItems;
            this.direction = direction;
            this.relationType = relationType;

            var result = localConfig.Globals.LState_Success.Clone();

            Abort();

            readDataAsync = new Thread(GetLogEntriesAsync);
            readDataAsync.Start();

            return result;
        }

        private void GetLogEntriesAsync()
        {
            var result = GetSubData_001_LogEntries();
            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultData = result;

            result = GetSubData_002_LogStates();
            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultData = result;

            result = GetSubData_003_Users();
            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultData = result;

            result = GetSubData_004_DateTimeStamps();
            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultData = result;

            ResultData = GetSubData_005_Messages();

        }

        private clsOntologyItem GetSubData_001_LogEntries()
        {
            List<clsObjectRel> searchLogEntries;

            if (direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                searchLogEntries = refItems.Select(refItem => new clsObjectRel
                {
                    ID_Object = refItem.GUID,
                    ID_RelationType = relationType.GUID,
                    ID_Parent_Other = localConfig.OItem_type_logentry.GUID
                }).ToList();
            }
            else
            {
                searchLogEntries = refItems.Select(refItem => new clsObjectRel
                {
                    ID_Other = refItem.GUID,
                    ID_RelationType = relationType.GUID,
                    ID_Parent_Object = localConfig.OItem_type_logentry.GUID
                }).ToList();
            }

            var result = dbReaderLogEntries.GetDataObjectRel(searchLogEntries);

            return result;

        }

        private clsOntologyItem GetSubData_002_LogStates()
        {
            List<clsObjectRel> searchLogStates;

            if (direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                searchLogStates = dbReaderLogEntries.ObjectRels.Select(logentry => new clsObjectRel
                {
                    ID_Object = logentry.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_provides.GUID,
                    ID_Parent_Other = localConfig.OItem_type_logstate.GUID
                }).ToList();
            }
            else
            {
                searchLogStates = dbReaderLogEntries.ObjectRels.Select(logentry => new clsObjectRel
                {
                    ID_Object = logentry.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_provides.GUID,
                    ID_Parent_Other = localConfig.OItem_type_logstate.GUID
                }).ToList();
            }

            if (!searchLogStates.Any())
            {
                dbReaderLogStates.ObjectRels.Clear();
                return localConfig.Globals.LState_Success.Clone();
            }

            var result = dbReaderLogStates.GetDataObjectRel(searchLogStates);

            return result;
        }

        private clsOntologyItem GetSubData_003_Users()
        {
            List<clsObjectRel> searchUsers;

            if (direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                searchUsers = dbReaderLogEntries.ObjectRels.Select(logentry => new clsObjectRel
                {
                    ID_Object = logentry.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_wascreatedby.GUID,
                    ID_Parent_Other = localConfig.OItem_type_user.GUID
                }).ToList();
            }
            else
            {
                searchUsers = dbReaderLogEntries.ObjectRels.Select(logentry => new clsObjectRel
                {
                    ID_Object = logentry.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_wascreatedby.GUID,
                    ID_Parent_Other = localConfig.OItem_type_user.GUID
                }).ToList();
            }

            if (!searchUsers.Any())
            {
                dbReaderUsers.ObjectRels.Clear();
                return localConfig.Globals.LState_Success.Clone();
            }

            var result = dbReaderUsers.GetDataObjectRel(searchUsers);

            return result;
        }

        private clsOntologyItem GetSubData_004_DateTimeStamps()
        {
            List<clsObjectAtt> searchDateTimeStamps;

            if (direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                searchDateTimeStamps = dbReaderLogEntries.ObjectRels.Select(logentry => new clsObjectAtt
                {
                    ID_Object = logentry.ID_Other,
                    ID_AttributeType = localConfig.OItem_attribute_datetimestamp.GUID
                }).ToList();
            }
            else
            {
                searchDateTimeStamps = dbReaderLogEntries.ObjectRels.Select(logentry => new clsObjectAtt
                {
                    ID_Object = logentry.ID_Object,
                    ID_AttributeType = localConfig.OItem_attribute_datetimestamp.GUID
                }).ToList();
            }

            if (!searchDateTimeStamps.Any())
            {
                dbReaderDateTimeStamps.ObjAtts.Clear();
                return localConfig.Globals.LState_Success.Clone();
            }

            var result = dbReaderDateTimeStamps.GetDataObjectAtt(searchDateTimeStamps);

            return result;
        }

        private clsOntologyItem GetSubData_005_Messages()
        {
            List<clsObjectAtt> searchMessages;

            if (direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                searchMessages = dbReaderLogEntries.ObjectRels.Select(logentry => new clsObjectAtt
                {
                    ID_Object = logentry.ID_Other,
                    ID_AttributeType = localConfig.OItem_attribute_message.GUID
                }).ToList();
            }
            else
            {
                searchMessages = dbReaderLogEntries.ObjectRels.Select(logentry => new clsObjectAtt
                {
                    ID_Object = logentry.ID_Object,
                    ID_AttributeType = localConfig.OItem_attribute_message.GUID
                }).ToList();
            }

            if (!searchMessages.Any())
            {
                dbReaderMessages.ObjAtts.Clear();
                return localConfig.Globals.LState_Success.Clone();
            }

            var result = dbReaderMessages.GetDataObjectAtt(searchMessages);

            return result;
        }

        public ElasticServiceAgent(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderLogEntries = new OntologyModDBConnector(localConfig.Globals);
            dbReaderLogStates = new OntologyModDBConnector(localConfig.Globals);
            dbReaderDateTimeStamps = new OntologyModDBConnector(localConfig.Globals);
            dbReaderMessages = new OntologyModDBConnector(localConfig.Globals);
            dbReaderUsers = new OntologyModDBConnector(localConfig.Globals);
        }

    }
}
