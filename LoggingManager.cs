﻿using LoggingServiceController.Factories;
using LoggingServiceController.Models;
using LoggingServiceController.Notifications;
using LoggingServiceController.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LoggingServiceController
{
    public class LoggingManager : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private object loggingLocker = new object();

        private ElasticServiceAgent elasticServiceAgent;
        private LogEntriesFactory logEntriesFactory;

        private clsTransaction transaction;
        private clsRelationConfig relationConfig;

        private List<clsOntologyItem> referenceItems;

        private clsOntologyItem oItemDirection;
        private clsOntologyItem oItemRelationType;

        public List<LogEntry> LogEntries { get; set; }

        private ResultChangeDateTimeStamp resultChangeDateTimeStamp;
        public ResultChangeDateTimeStamp ResultChangeDateTimeStamp
        {
            get { return resultChangeDateTimeStamp; }
            set
            {
                resultChangeDateTimeStamp = value;
                RaisePropertyChanged(nameof(ResultChangeDateTimeStamp));
            }
        }

        private ResultChangeDateTimeStamp resultCreateLogEntry;
        public ResultChangeDateTimeStamp ResultCreateLogEntry
        {
            get { return resultCreateLogEntry; }
            set
            {
                resultCreateLogEntry = value;
                RaisePropertyChanged(nameof(ResultCreateLogEntry));
            }
        }

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        public async Task<ResultChangeDateTimeStamp> ChangeLogEntryTime(LogEntry logEntry, DateTime dateTimeStamp)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            ResultChangeDateTimeStamp resultItem = null;

            lock (loggingLocker)
            {
                var rel = relationConfig.Rel_ObjectAttribute(new clsOntologyItem
                {
                    GUID = logEntry.IdLogEntry,
                    Name = logEntry.NameLogEntry,
                    GUID_Parent = localConfig.OItem_type_logentry.GUID,
                    Type = localConfig.Globals.Type_Object
                }, localConfig.OItem_attribute_datetimestamp, dateTimeStamp);

                transaction.ClearItems();
                result = transaction.do_Transaction(rel, true);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    logEntry.IdAttributeDateTimeStamp = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;
                    logEntry.DateTimeStamp = dateTimeStamp;
                }

                resultItem = new ResultChangeDateTimeStamp
                {
                    Result = result,
                    LogEntry = logEntry
                };
            }
            

            ResultChangeDateTimeStamp = resultItem;
            return resultItem;
        }

        public async Task<ResultChangeDateTimeStamp> CreateLogEntry(string logEntryName, clsOntologyItem logState, DateTime dateTimeStamp, clsOntologyItem oItemUser, string message = null, LogEntryRelation refItem = null)
        {
            ResultChangeDateTimeStamp resultItem = new ResultChangeDateTimeStamp
            {
                Result = localConfig.Globals.LState_Success.Clone(),
                LogEntry = new LogEntry()
            };
            
            lock (loggingLocker)
            {
                var oItemLogEntry = new clsOntologyItem
                {
                    GUID = localConfig.Globals.NewGUID,
                    Name = logEntryName,
                    GUID_Parent = localConfig.OItem_type_logentry.GUID,
                    Type = localConfig.Globals.Type_Object
                };

                transaction.ClearItems();
                resultItem.Result = transaction.do_Transaction(oItemLogEntry);

                if (resultItem.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    resultItem.LogEntry = new LogEntry
                    {
                        IdLogEntry = oItemLogEntry.GUID,
                        NameLogEntry = oItemLogEntry.Name
                    };

                    var relDateTimeStamp = relationConfig.Rel_ObjectAttribute(oItemLogEntry, localConfig.OItem_attribute_datetimestamp, dateTimeStamp);
                    resultItem.Result = transaction.do_Transaction(relDateTimeStamp, false);

                    if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        transaction.rollback();

                        ResultCreateLogEntry = resultItem;
                        return resultItem;
                    }

                    
                    resultItem.LogEntry.IdAttributeDateTimeStamp = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;
                    resultItem.LogEntry.DateTimeStamp = dateTimeStamp;

                    var relLogState = relationConfig.Rel_ObjectRelation(oItemLogEntry, logState, localConfig.OItem_relationtype_provides);

                    resultItem.Result = transaction.do_Transaction(relLogState, true);

                    
                    if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        transaction.rollback();

                        ResultCreateLogEntry = resultItem;
                        return resultItem;
                    }

                    
                    resultItem.LogEntry.IdLogState = transaction.OItem_Last.OItemObjectRel.ID_Other;
                    resultItem.LogEntry.NameLogState = transaction.OItem_Last.OItemObjectRel.Name_Other;

                    var relUser = relationConfig.Rel_ObjectRelation(oItemLogEntry, oItemUser, localConfig.OItem_relationtype_wascreatedby);

                    resultItem.Result = transaction.do_Transaction(relUser, true);

                    if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        transaction.rollback();

                        ResultCreateLogEntry = resultItem;
                        return resultItem;
                    }

                    resultItem.LogEntry.IdUser = oItemUser.GUID;
                    resultItem.LogEntry.NameUser = oItemUser.Name;

                    if (!string.IsNullOrEmpty(message))
                    {
                        var relMessage = relationConfig.Rel_ObjectAttribute(oItemLogEntry, localConfig.OItem_attribute_message, message);

                        resultItem.Result = transaction.do_Transaction(relMessage, true);

                        if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            transaction.rollback();

                            ResultCreateLogEntry = resultItem;
                            return resultItem;
                        }

                        resultItem.LogEntry.IdAttributeMessage = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;
                        resultItem.LogEntry.Message = message;
                    }

                    if (refItem != null && refItem.RefItem != null)
                    {
                        if (refItem.RelationType == null)
                        {
                            var relRef = relationConfig.Rel_ObjectRelation(oItemLogEntry, refItem.RefItem, localConfig.OItem_relationtype_belongsto);

                            resultItem.Result = transaction.do_Transaction(relRef, true);

                            if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                            {
                                transaction.rollback();

                                ResultCreateLogEntry = resultItem;
                                return resultItem;
                            }
                        }
                        else if (refItem.RelationType != null && refItem.Direction != null)
                        {
                            clsObjectRel relRef = null;

                            if (refItem.Direction.GUID == localConfig.Globals.Direction_LeftRight.GUID)
                            {
                                relRef = relationConfig.Rel_ObjectRelation(refItem.RefItem, oItemLogEntry,refItem.RelationType);
                            }
                            else
                            {
                                relRef = relationConfig.Rel_ObjectRelation(oItemLogEntry, refItem.RefItem, refItem.RelationType);
                            }

                            resultItem.Result = transaction.do_Transaction(relRef, true);

                            if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
                            {
                                transaction.rollback();

                                ResultCreateLogEntry = resultItem;
                                return resultItem;
                            }
                        }



                    }


                }
            }

            ResultCreateLogEntry = resultItem;
            return resultItem;
        }


        public clsOntologyItem GetRelatedLogEntries(List<clsOntologyItem> referenceItems, clsOntologyItem direction = null, clsOntologyItem relationType = null)
        {
            this.referenceItems = referenceItems;
            var result = localConfig.Globals.LState_Success.Clone();

            oItemDirection = direction;
            oItemRelationType = relationType;

            if (oItemDirection == null)
            {
                oItemDirection = localConfig.Globals.Direction_RightLeft;
            }

            if (oItemRelationType == null)
            {
                oItemRelationType = localConfig.OItem_relationtype_belongsto;
            }

            Abort();

            return elasticServiceAgent.GetLogEntries(referenceItems, direction != null ? direction : oItemDirection, relationType != null ? relationType : oItemRelationType);
        }


        public void Abort()
        {
            
            elasticServiceAgent.Abort();
        }

        public LoggingManager(Globals globals)
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            elasticServiceAgent = new ElasticServiceAgent(localConfig);
            elasticServiceAgent.PropertyChanged += ElasticServiceAgent_PropertyChanged;
            logEntriesFactory = new LogEntriesFactory(localConfig);

            transaction = new clsTransaction(localConfig.Globals);
            relationConfig = new clsRelationConfig(localConfig.Globals);
        }

        private void ElasticServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.Elastic_ResultData)
            {
                if (elasticServiceAgent.ResultData.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    LogEntries = logEntriesFactory.CreatelogEntries(oItemDirection,
                        elasticServiceAgent.LogEntries,
                        elasticServiceAgent.LogStates,
                        elasticServiceAgent.Users,
                        elasticServiceAgent.DateTimeStamps,
                        elasticServiceAgent.Messages);
                }
                

                ResultData = elasticServiceAgent.ResultData;
            }
        }
    }

    public class ResultChangeDateTimeStamp
    {
        public clsOntologyItem Result { get; set; }
        public LogEntry LogEntry { get; set; }
    }

    public class LogEntryRelation
    {
        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem RelationType { get; set; }
        public clsOntologyItem Direction { get; set; }

    }
}
