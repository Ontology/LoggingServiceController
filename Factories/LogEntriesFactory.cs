﻿using LoggingServiceController.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingServiceController.Factories
{
    public class LogEntriesFactory
    {

        private clsLocalConfig localConfig;

        public List<LogEntry> CreatelogEntries(clsOntologyItem oItemDirection,
            List<clsObjectRel> logEntries,
            List<clsObjectRel> logStates,
            List<clsObjectRel> users,
            List<clsObjectAtt> dateTimeStamps,
            List<clsObjectAtt> messages)
        {
            List<LogEntry> logCreatedLogEntries;
            if (oItemDirection.GUID == localConfig.Globals.Direction_LeftRight.GUID)
            {
                logCreatedLogEntries = (from logEntry in logEntries
                                        join logState in logStates on logEntry.ID_Other equals logState.ID_Object
                                        join user in users on logEntry.ID_Other equals user.ID_Object
                                        join dateTimeStamp in dateTimeStamps on logEntry.ID_Other equals dateTimeStamp.ID_Object
                                        join message in messages on logEntry.ID_Other equals message.ID_Object into messages1
                                        from message in messages1.DefaultIfEmpty()
                                        select new LogEntry
                                        {
                                            IdRef = logEntry.ID_Object,
                                            IdLogEntry = logEntry.ID_Other,
                                            NameLogEntry = logEntry.Name_Other,
                                            IdLogState = logState.ID_Other,
                                            NameLogState = logState.Name_Other,
                                            IdUser = user.ID_Other,
                                            NameUser = user.Name_Other,
                                            IdAttributeDateTimeStamp = dateTimeStamp.ID_Attribute,
                                            DateTimeStamp = dateTimeStamp.Val_Date,
                                            IdAttributeMessage = message != null ? message.ID_Attribute : null,
                                            Message = message != null ? message.Val_String : ""
                                        }).ToList();
            }
            else
            {
                logCreatedLogEntries = (from logEntry in logEntries
                                        join logState in logStates on logEntry.ID_Object equals logState.ID_Object
                                        join user in users on logEntry.ID_Object equals user.ID_Object
                                        join dateTimeStamp in dateTimeStamps on logEntry.ID_Object equals dateTimeStamp.ID_Object
                                        join message in messages on logEntry.ID_Object equals message.ID_Object into messages1
                                        from message in messages1.DefaultIfEmpty()
                                        select new LogEntry
                                        {
                                            IdRef = logEntry.ID_Other,
                                            IdLogEntry = logEntry.ID_Object,
                                            NameLogEntry = logEntry.Name_Object,
                                            IdLogState = logState.ID_Other,
                                            NameLogState = logState.Name_Other,
                                            IdUser = user.ID_Other,
                                            NameUser = user.Name_Other,
                                            IdAttributeDateTimeStamp = dateTimeStamp.ID_Attribute,
                                            DateTimeStamp = dateTimeStamp.Val_Date,
                                            IdAttributeMessage = message != null ? message.ID_Attribute : null,
                                            Message = message != null ? message.Val_String : ""
                                        }).ToList();
            }
            

            return logCreatedLogEntries;
        }


        public LogEntriesFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}
